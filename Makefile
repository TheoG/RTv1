# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tgros <tgros@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/28 14:36:13 by tgros             #+#    #+#              #
#    Updated: 2017/03/17 10:47:43 by tgros            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rtv1
LIBFT_NAME = libft.a
LIBMATHFT_NAME = libmathft.a

SRC_PATH = src
INC_PATH = inc/
OBJ_PATH = obj
LIB_PATH = libft/
SDL_PATH = SDL2/
LIBMATH_PATH = libmathft/

SRC_NAME = 	color \
			cone \
			cylinder \
			disk \
			file_controller \
			free \
			init_objects \
			light \
			lst_obj \
			main \
			parser \
			pixelmap \
			plane \
			rt_tools \
			rtv1 \
			sdl_rt \
			set_value_carac \
			set_value_coeff \
			set_value_trans \
			shading \
			sphere \
			unit_parser \
			util

SRC_NAME_PATH = $(addprefix $(OBJ_PATH), $(SRC_NAME))
SRC = $(addsuffix $(EXT), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/, $(SRC:.c=.o))

EXT = .c
CC	= gcc
ECHO = echo
FLG = -Werror -Wextra -Wall
SDL = `$(SDL_PATH)/sdl2-config --cflags --libs`

all: $(NAME)

$(OBJ_PATH)/%.o : $(SRC_PATH)/%.c
	@/bin/mkdir -p $(OBJ_PATH)
	@$(CC) $(FLG) -g -I$(INC_PATH) -I$(LIB_PATH) -I$(LIBMATH_PATH) -I$(SDL_PATH)/include -c -o $@ $^

$(NAME): $(OBJ)
	@$(ECHO) compiling SDL...
	@if [ ! -d "$(SDL_PATH)lib" ]; then \
		/bin/mkdir $(SDL_PATH)lib; \
		cd $(SDL_PATH) ; ./configure --prefix=`pwd`/lib; \
	fi
	@make -C $(SDL_PATH)
	@make -C $(SDL_PATH) install >/dev/null
	@make -C $(LIB_PATH)
	@make -C $(LIBMATH_PATH)
	@$(CC) $(FLG) -g $(SDL) $(LIB_PATH)$(LIBFT_NAME) $(LIBMATH_PATH)$(LIBMATHFT_NAME) $(OBJ) -o $(NAME)
	@$(ECHO) $(NAME) compilation done

clean:
	@/bin/rm -rf $(OBJ_PATH)
	@/bin/rm -rf $(SDL_PATH)lib
	@/bin/rm -rf $(SDL_PATH)build
	@$(ECHO) "SDL lib and build deleted"
	@make -C $(LIB_PATH) clean
	@make -C $(LIBMATH_PATH) clean
	@$(ECHO) $(NAME) clean done

fclean: clean
	@/bin/rm -f $(NAME)
	@make -C $(LIB_PATH) fclean
	@make -C $(LIBMATH_PATH) fclean
	@$(ECHO) $(NAME) fclean done

re: fclean all

PHONY : re all clean fclean
