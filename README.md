# RTv1

Premier Ray Tracer de l'école 42

### Concepts abordés
- Formules de quatre formes simples : Sphère, Plan, Cône et Cylindre ;
- Translations et rotations sur les 3 axes ;
- Déplacements et rotations de la caméra ;
- Blinn-Phong shadings ;
- Mélange des lumières et des ombres

### Exemple de description de scène
```
scene: Couleurs
{
	resolution: 1000, 1000
	ka: 0.1
	camera
	{
		position: 0, 10, 0
		rotation: 90, 0, 0
	}
	plane
	{
		position: 0, 0, 0
		direction: 0, 1, 0
		color: 0xAAAAAA
		ks: 0
		kd: 1
	}
	light
	{
		position: -0.5, 0.1, -0.5
		color: 0xFF0000
		intensity: 5
	}
	light
	{
		position: 0.5, 0.1, -0.5
		color: 0x00FF00
		intensity: 5
	}
	light
	{
		position: 0, 0.1, 0.5
		color: 0x0000FF
		intensity: 5
	}
	cylinder
	{
		position: 0, 0, 0
		height: 0.01
		radius: 1
	}
}
```

### Exemples d'images réalisées

![Scene pilones](https://www.dropbox.com/s/2dfb8232nrjcyiw/Screen%20Shot%202017-03-13%20at%2012.59.15%20PM.png?raw=1)

Exemple de mélange d'ombres et de spéculaire

![Melange des couleurs](https://www.dropbox.com/s/abl6ggd1l40lkiw/Screen%20Shot%202017-03-13%20at%204.49.47%20PM.png?raw=1)

Exemple de mélange des couleurs

![Tous les objets](https://www.dropbox.com/s/5lopmg8hyd6dhgg/Screen%20Shot%202017-03-13%20at%2012.24.51%20PM.png?raw=1)

Intersections entre tous les objets