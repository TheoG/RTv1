/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 14:35:48 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 14:28:23 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H

# include "libmathft.h"
# include "libft.h"

# include <SDL.h>
# include <fcntl.h>

# define MAX_X 2560
# define MAX_Y 1600
# define DIST 5.0
# define KD_DEF 1.0
# define KS_DEF 0.1
# define FOV_DEF 90.0
# define SPECULAR_EXPONENT 30
# define INTENSITY_DEF 10
# define BIAIS 0.00001
# define NB_SHAPE 6
# define NB_TOKEN 11

typedef enum		e_shapes
{
	PLANE,
	SPHERE,
	CYLINDER,
	CONE,
	DISK,
	CAMERA,
	LIGHT
}					t_shapes;

typedef enum		e_token
{
	POSITION,
	DIRECTION,
	ROTATION,
	COLOR,
	RADIUS,
	HEIGHT,
	KD,
	KS,
	SPEC_EXP,
	INTENSITY,
	FOV,
	END
}					t_token;

typedef struct		s_ray
{
	t_vec3			orig;
	t_vec3			dir;
	double			t;
}					t_ray;

typedef struct		s_hit
{
	t_vec3			hit;
	t_vec3			normal;
	t_ray			ray;
}					t_hit;

typedef struct		s_object
{
	t_shapes		type;
	t_vec3			pos;
	t_vec3			dir;
	t_vec3			rot;
	t_matrix4		m_ori;
	t_matrix4		m_inv;
	t_color			color;
	int				n_dir;
	char			*name;
	double			fov;
	double			radius;
	double			height;
	double			kd;
	double			ks;
	double			spec_exp;
	double			intensity;
	int				(*inter)(struct s_object *obj, t_vec3 orig, t_vec3 dir,
						t_hit *t);
	t_vec3			(*get_normal)(struct s_object *obj, t_hit *hit);
	struct s_object	*next;
}					t_object;

typedef struct		s_scene
{
	t_object		*list_obj;
	t_object		*list_light;
	t_object		*list_camera;
	int				bg;
	int				rec;
	t_pt2			res;
	char			*name;
	float			ka;
}					t_scene;

typedef	struct		s_rt
{
	int				(*inter[NB_SHAPE])(struct s_object *obj, t_vec3 orig,
						t_vec3 dir, t_hit *t);
	t_vec3			(*normal[NB_SHAPE])(struct s_object *obj, t_hit *hit);
	char			*token[END + 1];
	char			*shapes_name[NB_SHAPE + 1];
	void			(*set_value[END + 1])(t_object *obj, char *data, int line);
}					t_rt;

void				rtv1(char *file);
void				init_tools(t_rt **tools);

/*
**	Parser (file_controller.c, parser.c, unit_parser.c, lst_obj.c, init_objects)
*/

int					open_file(char *file);
t_list				*read_file(int fd);

t_scene				*parse_file(char *name, t_list *file_content, t_rt *tools);
void				extract_camera_light(t_scene *scene);

t_vec3				read_vec3(char *str);
t_pt2				read_pt2(char *str);

void				obj_pushback(t_object **head, t_object *new_obj);
t_object			*acquire_elem(t_object **head, t_object *new_obj);

t_object			*init_object(t_list **file, char *name, int type,
						t_rt *tools);
t_object			*cylinder_caps(t_object *cylinder, int is_top);
void				add_caps(t_scene *scene);
void				init_matrices(t_object *object);

/*
**	util.c, color.c
*/

int					ft_strarraylength(char *tab[]);
void				display_percentage(int percent);

int					hex_to_int(char *num);
int					rgb_to_int(t_color rgb);
t_color				int_to_rgb(int color);

/*
**	Renderer (pixelmap.c, sdl_rt.c)
*/

int					**init_pixelmap(t_scene *scene);

int					sdl_render(int **pixelmap, char *name, int x, int y);

/*
**	Shapes
*/

int					inter_sphere(t_object *obj, t_vec3 o, t_vec3 dir, t_hit *t);
int					inter_plane(t_object *obj, t_vec3 o, t_vec3 dir, t_hit *t);
int					inter_cylinder(t_object *ob, t_vec3 o, t_vec3 d, t_hit *t);
int					inter_cone(t_object *obj, t_vec3 o, t_vec3 dir, t_hit *t);
int					inter_disk(t_object *obj, t_vec3 o, t_vec3 dir, t_hit *t);

t_vec3				norme_sphere(t_object *obj, t_hit *hit);
t_vec3				norme_plane(t_object *obj, t_hit *hit);
t_vec3				norme_cylinder(t_object *obj, t_hit *hit);
t_vec3				norme_cone(t_object *obj, t_hit *hit);
t_vec3				norme_disk(t_object *obj, t_hit *hit);

/*
**	shading.c
*/

int					get_ambient(t_color *c, t_object *obj, t_scene *scene);
int					get_shadow(t_scene *sc, t_object *obj, t_hit *hit,
						t_object *l);
int					get_diffuse(t_color *c, t_object *obj, t_hit *hit,
						t_object *l);
int					get_specular(t_color *c, t_object *obj, t_hit *hit,
						t_object *l);

void				free_everything(t_scene *scene, int **pixelmap);
void				free_list(t_list *file);
void				free_split(char **split);
void				free_pixelmap(int y, int **pixelmap);
void				free_listobj(t_object *head);

/*
**	set_value.c
*/

void				set_position(t_object *obj, char *data, int line);
void				set_direction (t_object *obj, char *data, int line);
void				set_rotation (t_object *obj, char *data, int line);
void				set_color (t_object *obj, char *data, int line);
void				set_radius (t_object *obj, char *data, int line);
void				set_height (t_object *obj, char *data, int line);
void				set_kd (t_object *obj, char *data, int line);
void				set_ks (t_object *obj, char *data, int line);
void				set_spec_exp (t_object *obj, char *data, int line);
void				set_intensity (t_object *obj, char *data, int line);
void				set_fov (t_object *obj, char *data, int line);
void				check_values(t_scene *scene);

#endif
