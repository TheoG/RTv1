/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_exit_line.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 14:17:28 by tgros             #+#    #+#             */
/*   Updated: 2017/02/10 14:20:10 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Puts the error message on the error output and exits the program.
*/

#include "libft.h"

void	ft_error_exit_line(char *error_msg, int line)
{
	ft_putstr_fd(error_msg, 2);
	ft_putstr_fd(" at line ", 2);
	ft_putnbr_fd(line, 2);
	ft_putendl("");
	exit(-1);
}
