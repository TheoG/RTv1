/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtolower.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 11:32:19 by tgros             #+#    #+#             */
/*   Updated: 2017/03/12 11:21:55 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Puts the uppercase chars in a char string to lowercase.
*/

char	*ft_strtolower(char *s)
{
	int	i;

	i = -1;
	while (s && s[++i])
		ft_isupper(s[i]) ? s[i] = ft_tolower(s[i]) : 0;
	return (s);
}
