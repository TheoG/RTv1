/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_abs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/26 15:07:25 by tgros             #+#    #+#             */
/*   Updated: 2017/01/26 15:07:27 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

t_complex	c_abs(t_complex x)
{
	x.r < 0 ? x.r *= -1 : 0;
	x.i < 0 ? x.i *= -1 : 0;
	return (x);
}
