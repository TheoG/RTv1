/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix4_translation.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 13:36:43 by tgros             #+#    #+#             */
/*   Updated: 2017/03/05 13:36:44 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Combines a matrix and a translation vector.
*/

t_matrix4	m4_trans(t_matrix4 m, t_vec3 v)
{
	m[0][3] += v.x;
	m[1][3] += v.y;
	m[2][3] += v.z;
	return (m);
}
