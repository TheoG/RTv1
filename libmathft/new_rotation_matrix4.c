/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_rotation_matrix4.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:55:34 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:45:49 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

static t_matrix4	m_rot_x(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0][0] = 1;
	m[1][1] = cos(to_radian(angle));
	m[1][2] = sin(to_radian(angle));
	m[2][1] = -sin(to_radian(angle));
	m[2][2] = cos(to_radian(angle));
	return (m);
}

static t_matrix4	m_rot_y(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0][0] = cos(to_radian(angle));
	m[0][2] = -sin(to_radian(angle));
	m[1][1] = 1;
	m[2][0] = sin(to_radian(angle));
	m[2][2] = cos(to_radian(angle));
	return (m);
}

static t_matrix4	m_rot_z(double angle)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0][0] = cos(to_radian(angle));
	m[0][1] = sin(to_radian(angle));
	m[1][0] = -sin(to_radian(angle));
	m[1][1] = cos(to_radian(angle));
	m[2][2] = 1;
	return (m);
}

/*
** Creates a new rotation matrix of angle 'angle' around axis 'axis'
*/

t_matrix4			new_rotation_matrix4(double angle, char axis)
{
	if (axis == 'x' || axis == 'X')
		return (m_rot_x(angle));
	else if (axis == 'y' || axis == 'Y')
		return (m_rot_y(angle));
	else if (axis == 'z' || axis == 'Z')
		return (m_rot_z(angle));
	return (0);
}
