/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_scaling_matrix4.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 10:56:20 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:45:53 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Creates a new scaling matrix which will scale a matrix or vector of all
** coordinates by i.
*/

t_matrix4	new_scaling_matrix4(double i)
{
	t_matrix4	m;

	m = new_matrix4();
	m[0][0] = i;
	m[1][1] = i;
	m[2][2] = i;
	return (m);
}
