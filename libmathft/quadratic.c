/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quadratic.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 14:24:12 by tgros             #+#    #+#             */
/*   Updated: 2017/03/14 11:27:51 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

int		quadratic(double a, double b, double c, t_vec2 *res)
{
	double	delta;

	delta = b * b - 4.0 * a * c;
	if (delta < 0.0)
		return (0);
	else if (fabs(delta) < 0.000000000001)
	{
		res->x = -b / (double)(2.0 * a);
		res->y = res->x;
	}
	else
	{
		res->x = (-b - sqrt(delta)) / (double)(2.0 * a);
		res->y = (-b + sqrt(delta)) / (double)(2.0 * a);
	}
	return (1);
}
