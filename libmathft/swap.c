/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 10:31:37 by tgros             #+#    #+#             */
/*   Updated: 2017/02/26 14:56:19 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

void	double_swap(double *f1, double *f2)
{
	double tmp;

	tmp = *f1;
	*f1 = *f2;
	*f2 = tmp;
}
