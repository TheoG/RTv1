/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_dot.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 13:37:48 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:46:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
**	Dot product between vec1 and vec2
*/

double	v3_dot(t_vec3 vec1, t_vec3 vec2)
{
	return (vec1.x * vec2.x +
			vec1.y * vec2.y +
			vec1.z * vec2.z);
}
