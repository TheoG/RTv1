/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_matrix4_product.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 13:38:00 by tgros             #+#    #+#             */
/*   Updated: 2017/03/05 13:38:02 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmathft.h"

/*
** Returns the product of matrix m and vector v.
*/

t_vec3	v3_m4_prod(t_vec3 v, t_matrix4 m)
{
	t_vec3	new;

	new.x = v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + m[3][0];
	new.y = v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + m[3][1];
	new.z = v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + m[3][2];
	return (new);
}
