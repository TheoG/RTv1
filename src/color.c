/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/08 14:45:41 by tgros             #+#    #+#             */
/*   Updated: 2017/03/08 13:46:47 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

/*
**	Convert a hex string (0x... format) to int
*/

int		hex_to_int(char *num)
{
	int len;
	int res;
	int ind;

	if (!num)
		return (-1);
	ind = 0;
	res = 0;
	len = ft_strlen(num);
	len--;
	while (ft_toupper(num[len]) != 'X')
	{
		if (num[len] >= '0' && num[len] <= '9')
			res += (num[len] - '0') * pow(16, ind);
		else if (ft_toupper(num[len]) >= 'A' && ft_toupper(num[len]) <= 'F')
			res += (ft_toupper(num[len]) - 'A' + 10) * pow(16, ind);
		ind++;
		len--;
	}
	return (res);
}

/*
**	Takes a hex (int) color and return the rgb value
*/

t_color	int_to_rgb(int color)
{
	t_color rgb;

	rgb.r = (color & 0x00FF0000) >> 16;
	rgb.g = (color & 0x0000FF00) >> 8;
	rgb.b = (color & 0x000000FF);
	return (rgb);
}

/*
**	Takes an rgb format in argument and return the hexa (int) value
*/

int		rgb_to_int(t_color rgb)
{
	int r;
	int g;
	int b;

	r = ((int)rgb.r & 0xff) << 16;
	g = ((int)rgb.g & 0xff) << 8;
	b = ((int)rgb.b & 0xff);
	return (r + g + b);
}
