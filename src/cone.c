/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 15:43:49 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 10:24:55 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		inter_cone(t_object *obj, t_vec3 orig, t_vec3 dir, t_hit *t)
{
	t_vec3	ori_cen;
	t_vec3	u;
	t_vec2	res;
	double	k;

	orig = v3_m4_prod(orig, obj->m_ori);
	dir = v3_m4_prod(dir, obj->m_ori);
	k = obj->radius / (double)obj->height;
	ori_cen = v3_sub(orig, obj->pos);
	u.x = v3_dot(dir, dir) - (1.0 + k * k) * pow(v3_dot(dir, obj->dir), 2.0);
	u.y = 2 * (v3_dot(dir, ori_cen) - (1.0 + k * k) * v3_dot(dir, obj->dir)
				* v3_dot(ori_cen, obj->dir));
	u.z = v3_dot(ori_cen, ori_cen) - (1.0 + k * k) * pow(v3_dot(ori_cen,
				obj->dir), 2.0);
	if (!quadratic(u.x, u.y, u.z, &res))
		return (0);
	res.x > res.y ? double_swap(&(res.x), &(res.y)) : 0;
	if (res.x < 0.0)
	{
		res.x = res.y;
		if (res.y < 0.0)
			return (0);
	}
	t->ray.t = res.x;
	return (1);
}

t_vec3	norme_cone(t_object *obj, t_hit *hit)
{
	t_vec3	v;
	t_vec3	hit_p;

	hit_p = v3_m4_prod(hit->hit, obj->m_ori);
	v.x = (hit_p.x - obj->pos.x) * (obj->height / obj->radius);
	v.y = obj->radius / obj->height;
	v.z = (hit_p.z - obj->pos.z) * (obj->height / obj->radius);
	v = v3_nrm(v);
	return (v3_m4_prod(v3_prod(v, obj->n_dir), obj->m_inv));
}
