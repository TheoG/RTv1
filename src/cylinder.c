/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 14:28:05 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 14:39:29 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int		check_caps(t_object *obj, t_vec3 *dir, t_vec2 *res, t_vec3 *o_c)
{
	t_vec3	dir_u;
	t_vec2	m;

	dir_u.x = 0;
	dir_u.y = 1;
	dir_u.z = 0;
	m.x = v3_dot(*dir, v3_prod(dir_u, res->x)) + v3_dot(*o_c, dir_u);
	m.y = v3_dot(*dir, v3_prod(dir_u, res->y)) + v3_dot(*o_c, dir_u);
	if (m.x < 0.00000000f || m.x > obj->height)
	{
		m.x = m.y;
		res->x = res->y;
		if (m.x < 0.00000000f || m.x > obj->height)
			return (0);
	}
	return (1);
}

int				inter_cylinder(t_object *obj, t_vec3 orig, t_vec3 dir, t_hit *t)
{
	t_vec3	u;
	t_vec2	res;
	t_vec3	o_c;

	orig = v3_m4_prod(orig, obj->m_ori);
	dir = v3_m4_prod(dir, obj->m_ori);
	o_c = v3_sub(orig, obj->pos);
	u.x = (dir.x * dir.x) + (dir.z * dir.z);
	u.y = 2 * ((dir.x * o_c.x) + (dir.z * o_c.z));
	u.z = o_c.x * o_c.x + o_c.z * o_c.z - obj->radius * obj->radius;
	if (!quadratic(u.x, u.y, u.z, &res))
		return (0);
	if (res.x > res.y)
		double_swap(&(res.x), &(res.y));
	if (!check_caps(obj, &dir, &res, &o_c))
		return (0);
	t->ray.t = res.x;
	return (1);
}

t_vec3			norme_cylinder(t_object *obj, t_hit *hit)
{
	t_vec3	n;

	n = v3_sub(v3_m4_prod(hit->hit, obj->m_ori), obj->pos);
	n.y = 0.0;
	n = v3_m4_prod(n, obj->m_inv);
	return (n);
}

t_object		*cylinder_caps(t_object *object, int is_top)
{
	t_object	*cap1;

	if (!(cap1 = (t_object *)malloc(sizeof(t_object))))
		ft_error_exit("Malloc error");
	ft_bzero(cap1, sizeof(t_object));
	cap1->color = object->color;
	cap1->dir.y = 1;
	cap1->pos = object->pos;
	cap1->rot = object->rot;
	if (is_top)
		cap1->pos.y += object->height;
	cap1->radius = object->radius;
	cap1->ks = object->ks;
	cap1->kd = object->kd;
	cap1->spec_exp = object->spec_exp;
	cap1->inter = &inter_disk;
	cap1->get_normal = &norme_disk;
	cap1->type = DISK;
	init_matrices(cap1);
	return (cap1);
}
