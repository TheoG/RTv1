/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disk.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 10:16:08 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 12:02:10 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int			inter_disk(t_object *obj, t_vec3 orig, t_vec3 dir, t_hit *t)
{
	t_vec3	p;
	t_vec3	v;

	if (inter_plane(obj, orig, dir, t))
	{
		orig = v3_m4_prod(orig, obj->m_ori);
		dir = v3_m4_prod(dir, obj->m_ori);
		p = v3_add(orig, v3_prod(dir, t->ray.t));
		v = v3_sub(p, obj->pos);
		if (v3_dot(v, v) <= obj->radius * obj->radius)
			return (1);
	}
	return (0);
}

t_vec3		norme_disk(t_object *obj, t_hit *hit)
{
	(void)hit;
	return (v3_m4_prod(v3_prod(obj->dir, obj->n_dir), obj->m_inv));
}
