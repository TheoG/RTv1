/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_controller.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 12:07:18 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 15:26:29 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		open_file(char *file)
{
	int	fd;

	if ((fd = open(file, O_RDONLY)) < 0)
	{
		perror(file);
		exit(1);
	}
	return (fd);
}

t_list	*read_file(int fd)
{
	char	*line;
	t_list	*head;
	t_list	*current_line;
	int		i;

	head = NULL;
	line = NULL;
	i = 1;
	while (get_next_line(fd, &line) == 1)
	{
		if (!line || !(current_line = ft_lstnew(line, ft_strlen(line) + 1)))
			ft_error_exit("Malloc error");
		free(line);
		line = NULL;
		current_line->content_size = i;
		current_line->next = NULL;
		if (!head)
			head = current_line;
		else
			ft_lstadd_end(head, current_line);
		current_line = current_line->next;
		i++;
	}
	close(fd);
	return (head);
}
