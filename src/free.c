/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 11:01:19 by tgros             #+#    #+#             */
/*   Updated: 2017/03/11 13:16:07 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	free_pixelmap(int y, int **pixelmap)
{
	int	i;

	i = -1;
	while (++i < y)
		free(pixelmap[i]);
	free(pixelmap);
}

void	free_listobj(t_object *head)
{
	t_object	*current;
	t_object	*del;

	current = head;
	while (current)
	{
		del = current;
		current = current->next;
		if (del->name)
			free(del->name);
		if (del->m_ori)
			matrix4_free(del->m_ori);
		if (del->m_inv)
			matrix4_free(del->m_inv);
		free(del);
	}
}

void	free_everything(t_scene *scene, int **pixelmap)
{
	free_pixelmap(scene->res.y, pixelmap);
	free_listobj(scene->list_obj);
	free_listobj(scene->list_camera);
	free_listobj(scene->list_light);
	free(scene->name);
	free(scene);
}

void	free_split(char **split)
{
	int i;

	i = 0;
	while (split[i])
	{
		free(split[i]);
		i++;
	}
	free(split);
}

void	free_list(t_list *file)
{
	t_list	*current;
	t_list	*del;

	current = file;
	while (current)
	{
		del = current;
		current = current->next;
		free(del->content);
		free(del);
	}
}
