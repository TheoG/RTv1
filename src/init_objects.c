/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_objects.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 15:22:01 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 15:53:39 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	find_token(char *file, t_object **object, t_rt *tools, int line)
{
	char	**split;
	char	*trim;
	char	**tmp;
	int		i;

	split = ft_strsplit(file, ':');
	if (!(*split) || !split)
		ft_error_exit("Bad formatting");
	if (!(trim = ft_strtoupper(ft_strtrim(split[0]))))
		ft_error_exit("Malloc error");
	tmp = tools->token;
	i = 0;
	while (tmp[i])
	{
		if (ft_strcmp(trim, tools->token[i]) == 0)
		{
			tools->set_value[i](*object, *(split + 1), line);
			break ;
		}
		i++;
	}
	free_split(split);
	free(trim);
	return (tmp[i] != NULL);
}

static void	init_values(t_object **object)
{
	ft_bzero(*object, sizeof(t_object));
	(*object)->kd = KD_DEF;
	(*object)->ks = KS_DEF;
	(*object)->spec_exp = SPECULAR_EXPONENT;
	(*object)->fov = FOV_DEF;
	(*object)->intensity = INTENSITY_DEF;
	(*object)->color = int_to_rgb(0xFFFFFF);
	(*object)->n_dir = 1;
}

void		init_matrices(t_object *object)
{
	t_matrix4	t_x;
	t_matrix4	t_y;
	t_matrix4	t_z;
	t_matrix4	t_xy;

	if (object->type == LIGHT)
		return ;
	t_x = new_rotation_matrix4(object->rot.x, 'x');
	t_y = new_rotation_matrix4(object->rot.y, 'y');
	t_z = new_rotation_matrix4(object->rot.z, 'z');
	t_xy = m4_prod(t_x, t_y);
	object->m_ori = m4_trans(m4_prod(t_z, t_xy), object->pos);
	matrix4_free(t_x);
	matrix4_free(t_y);
	matrix4_free(t_z);
	matrix4_free(t_xy);
	t_x = new_rotation_matrix4(-object->rot.x, 'x');
	t_y = new_rotation_matrix4(-object->rot.y, 'y');
	t_z = new_rotation_matrix4(-object->rot.z, 'z');
	t_xy = m4_prod(t_x, t_y);
	object->m_inv = m4_trans(m4_prod(t_z, t_xy), object->pos);
	matrix4_free(t_x);
	matrix4_free(t_y);
	matrix4_free(t_z);
	matrix4_free(t_xy);
}

void		add_caps(t_scene *scene)
{
	t_object *obj;

	obj = scene->list_obj;
	while (obj)
	{
		if (obj->type == CYLINDER)
		{
			obj_pushback(&(scene->list_obj), cylinder_caps(obj, 1));
			obj_pushback(&(scene->list_obj), cylinder_caps(obj, 0));
		}
		obj = obj->next;
	}
}

t_object	*init_object(t_list **file, char *name, int type,
				t_rt *tools)
{
	t_object	*obj;

	(*file) = (*file)->next;
	if (!(*file) || !ft_strchr((*file)->content, '{') || !(*file)->next)
		ft_error_exit("Bad formatting");
	(*file) = (*file)->next;
	if (!(obj = (t_object *)malloc(sizeof(t_object))))
		ft_error_exit("Malloc error");
	init_values(&obj);
	obj->name = name;
	obj->type = type;
	obj->inter = tools->inter[type];
	obj->get_normal = tools->normal[type];
	while ((*file))
	{
		if (!(*file) || !(*file)->content || !(*file)->next)
			ft_error_exit("Bad formatting");
		if (ft_strchr((*file)->content, '}'))
			break ;
		if (!find_token((*file)->content, &obj, tools, (*file)->content_size))
			ft_error_exit_line("Undefined identifier", (*file)->content_size);
		(*file) = (*file)->next;
	}
	init_matrices(obj);
	return (obj);
}
