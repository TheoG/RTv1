/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_obj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 14:23:57 by tgros             #+#    #+#             */
/*   Updated: 2017/03/05 14:32:29 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void		obj_pushback(t_object **head, t_object *new)
{
	t_object	*tmp;

	if (!*head && !new)
		return ;
	if (!*head)
	{
		*head = new;
		(*head)->next = NULL;
		return ;
	}
	tmp = *head;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
	new->next = NULL;
}

t_object	*acquire_elem(t_object **head, t_object *obj)
{
	t_object	*prev;
	t_object	*tmp;

	if (!obj)
		return (NULL);
	if (obj == *head)
	{
		*head = (*head)->next;
		return (obj);
	}
	prev = (*head);
	tmp = (*head)->next;
	while (tmp && tmp != obj)
	{
		prev = prev->next;
		tmp = tmp->next;
	}
	if (tmp == obj)
	{
		prev->next = tmp->next;
		tmp->next = NULL;
		return (tmp);
	}
	return (NULL);
}
