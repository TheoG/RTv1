/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 11:59:47 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 11:39:17 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	usage(void)
{
	ft_putendl_fd("usage: ./rtv1 <scene>", 2);
	exit(1);
}

int			main(int argc, char *argv[])
{
	if (argc != 2)
		usage();
	rtv1(argv[1]);
	return (0);
}
