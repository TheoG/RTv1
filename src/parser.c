/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 11:45:31 by tgros             #+#    #+#             */
/*   Updated: 2017/03/17 10:37:30 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int			check_camera_light(t_list **file, t_scene *scene, t_rt *tools)
{
	char		**split;
	char		*trim;
	char		*name;
	int			ret;

	ret = 1;
	if (!(split = ft_strsplit((*file)->content, ':')) || !(*split))
		ft_error_exit("Bad formatting");
	(trim = ft_strtrim(split[0])) ? 0 : ft_error_exit("Malloc error");
	name = *(split + 1) ? ft_strdup(*(split + 1)) : NULL;
	trim = ft_strtoupper(trim);
	if (ft_strcmp(ft_strtoupper(trim), "CAMERA") == 0)
		obj_pushback(&(scene->list_camera),
			init_object(file, name, CAMERA, tools));
	else if (ft_strcmp(ft_strtoupper(trim), "LIGHT") == 0)
		obj_pushback(&(scene->list_light),
			init_object(file, name, LIGHT, tools));
	else
	{
		ret = 0;
		free(name);
	}
	free_split(split);
	free(trim);
	return (ret);
}

int			check_obj(t_list **file, t_scene *scene, t_rt *tools)
{
	char		**split;
	char		*trim;
	char		*name;
	int			ret;
	int			i;

	ret = 0;
	i = 0;
	split = ft_strsplit((*file)->content, ':');
	if (!(*split))
		ft_error_exit("Bad formatting");
	(trim = ft_strtrim(split[0])) ? 0 : ft_error_exit("Malloc error");
	name = (*(split + 1)) ? ft_strdup((*(split + 1))) : NULL;
	trim = ft_strtoupper(trim);
	while (tools->shapes_name[i] && ret == 0)
	{
		if (ft_strcmp(trim, tools->shapes_name[i]) == 0 && (ret = 1))
			obj_pushback(&(scene->list_obj), init_object(file, name, i, tools));
		i++;
	}
	free_split(split);
	free(trim);
	return (ret);
}

int			check_scene_param(t_list **file, t_scene **scene)
{
	char		**split;
	char		*trim;
	int			ret;

	ret = 1;
	split = ft_strsplit((*file)->content, ':');
	if (!split || !(*split))
		ft_error_exit("Bad formatting");
	(trim = ft_strtrim(split[0])) ? 0 : ft_error_exit("Malloc error");
	if (ft_strcmp(ft_strtoupper(trim), "RECURSION") == 0)
		(*scene)->rec = ft_atoi((*(split + 1)) ? (*(split + 1)) : 0);
	else if (ft_strcmp(ft_strtoupper(trim), "RESOLUTION") == 0)
		(*scene)->res = read_pt2((*(split + 1)) ? (*(split + 1)) : 0);
	else if (ft_strcmp(ft_strtoupper(trim), "KA") == 0)
		(*scene)->ka = ft_atof(((*(split + 1)) ? (*(split + 1)) : 0));
	else if (ft_strcmp(ft_strtoupper(trim), "BACKGROUND COLOR") == 0)
		(*scene)->bg = hex_to_int(((*(split + 1)) ? (*(split + 1)) : 0));
	else
		ret = 0;
	ft_atof(0);
	free_split(split);
	free(trim);
	return (ret);
}

/*
**	Parse only one scene
**	@file: the current scene, right after the "scene" declaration
**	@return The new scene
*/

t_scene		*parse_scene(t_list **file, t_rt *tools)
{
	t_scene		*scene;

	*file = (*file)->next;
	if (!file || !(*file) || ft_strchr((*file)->content, '{') == 0
		|| !(*file)->next)
		ft_error_exit("Bad formatting");
	(*file) = (*file)->next;
	if (!(scene = (t_scene *)malloc(sizeof(t_scene))))
		ft_error_exit("Malloc error");
	ft_bzero(scene, sizeof(t_scene));
	while (ft_strchr((*file)->content, '}') == 0)
	{
		if (!(*file) || !(*file)->content)
			ft_error_exit("Bad formatting");
		if (check_scene_param(file, &scene))
			(*file) = (*file)->next;
		else if (check_camera_light(file, scene, tools))
			(*file) = (*file)->next;
		else if (check_obj(file, scene, tools))
			(*file) = (*file)->next;
		else
			ft_error_exit_line("Undefined identifier", (*file)->content_size);
	}
	check_values(scene);
	return (scene);
}

/*
**	Parse the entire file
**	@param file: linked list that contains the whole file
**	@return: A list of scenes that contains a list of objects,
**		cameras and lights, and caracteristics like the scene size, etc.
*/

t_scene		*parse_file(char *name, t_list *file, t_rt *tools)
{
	t_scene	*scene;
	t_list	*beg_file;
	char	**split;
	char	*trim;

	scene = NULL;
	beg_file = file;
	while (file)
	{
		if (!(split = ft_strsplit(file->content, ':')) || !(*split))
			ft_error_exit("Bad formatting");
		(trim = ft_strtrim(split[0])) ? 0 : ft_error_exit("Malloc error");
		if (ft_strcmp(ft_strtoupper(trim), "SCENE") == 0)
			scene = parse_scene(&file, tools);
		else
			ft_error_exit_line("Undefined identifier", file->content_size);
		free_split(split);
		free(trim);
		file = file->next;
	}
	free_list(beg_file);
	if (!(scene->name = ft_strnew(ft_strlen("RTv1: ") + ft_strlen(name))))
		ft_error_exit("Malloc error");
	return (scene);
}
