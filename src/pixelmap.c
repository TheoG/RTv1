/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixelmap.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 15:36:44 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 14:40:53 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int	**init_pixelmap(t_scene *scene)
{
	int	**pixelmap;
	int	i;

	i = -1;
	if (!(pixelmap = (int **)malloc(sizeof(int *) * scene->res.y)))
		ft_error_exit("Malloc error");
	while (++i < scene->res.y)
	{
		if (!(pixelmap[i] = (int *)malloc(sizeof(int) * scene->res.x)))
			ft_error_exit("Malloc error");
	}
	return (pixelmap);
}
