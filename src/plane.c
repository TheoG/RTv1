/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/04 11:23:56 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 12:02:28 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	check_side(t_object *obj, t_vec3 dir)
{
	double		den;

	den = v3_dot(v3_m4_prod(obj->dir, obj->m_inv), dir);
	if (den > 0.0)
		obj->n_dir = -1;
	else
		obj->n_dir = 1;
}

int			inter_plane(t_object *obj, t_vec3 orig, t_vec3 dir, t_hit *t)
{
	double	den;
	t_vec3	ori_cen;

	check_side(obj, dir);
	orig = v3_m4_prod(orig, obj->m_ori);
	dir = v3_m4_prod(dir, obj->m_ori);
	den = v3_dot(v3_prod(obj->dir, obj->n_dir), dir);
	ori_cen = v3_sub(obj->pos, orig);
	t->ray.t = v3_dot(ori_cen, v3_prod(obj->dir, obj->n_dir)) / (double)den;
	return (t->ray.t >= 0 ? 1 : 0);
}

t_vec3		norme_plane(t_object *obj, t_hit *hit)
{
	(void)hit;
	return (v3_prod(v3_m4_prod(obj->dir, obj->m_inv), obj->n_dir));
}
