/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 17:04:26 by tgros             #+#    #+#             */
/*   Updated: 2017/03/08 14:50:15 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		init_inters_normals(t_rt **tools)
{
	(*tools)->inter[PLANE] = &inter_plane;
	(*tools)->inter[SPHERE] = &inter_sphere;
	(*tools)->inter[CYLINDER] = &inter_cylinder;
	(*tools)->inter[CONE] = &inter_cone;
	(*tools)->inter[DISK] = &inter_disk;
	(*tools)->normal[PLANE] = &norme_plane;
	(*tools)->normal[SPHERE] = &norme_sphere;
	(*tools)->normal[CYLINDER] = &norme_cylinder;
	(*tools)->normal[DISK] = &norme_disk;
	(*tools)->normal[CONE] = &norme_cone;
}

static void		init_tokens(t_rt **tools)
{
	(*tools)->token[POSITION] = "POSITION";
	(*tools)->token[DIRECTION] = "DIRECTION";
	(*tools)->token[ROTATION] = "ROTATION";
	(*tools)->token[COLOR] = "COLOR";
	(*tools)->token[RADIUS] = "RADIUS";
	(*tools)->token[HEIGHT] = "HEIGHT";
	(*tools)->token[KD] = "KD";
	(*tools)->token[KS] = "KS";
	(*tools)->token[SPEC_EXP] = "SPEC_EXP";
	(*tools)->token[INTENSITY] = "INTENSITY";
	(*tools)->token[FOV] = "FOV";
	(*tools)->token[END] = NULL;
}

static void		init_validators(t_rt **tools)
{
	(*tools)->set_value[POSITION] = &set_position;
	(*tools)->set_value[DIRECTION] = &set_direction;
	(*tools)->set_value[ROTATION] = &set_rotation;
	(*tools)->set_value[COLOR] = &set_color;
	(*tools)->set_value[RADIUS] = &set_radius;
	(*tools)->set_value[HEIGHT] = &set_height;
	(*tools)->set_value[KD] = &set_kd;
	(*tools)->set_value[KS] = &set_ks;
	(*tools)->set_value[SPEC_EXP] = &set_spec_exp;
	(*tools)->set_value[INTENSITY] = &set_intensity;
	(*tools)->set_value[FOV] = &set_fov;
}

static void		init_shapes_name(t_rt **tools)
{
	(*tools)->shapes_name[PLANE] = "PLANE";
	(*tools)->shapes_name[SPHERE] = "SPHERE";
	(*tools)->shapes_name[CYLINDER] = "CYLINDER";
	(*tools)->shapes_name[CONE] = "CONE";
	(*tools)->shapes_name[DISK] = "DISK";
	(*tools)->shapes_name[NB_SHAPE] = NULL;
}

void			init_tools(t_rt **tools)
{
	if (!((*tools) = (t_rt *)malloc(sizeof(t_rt))))
		ft_error_exit("Malloc error");
	init_inters_normals(tools);
	init_tokens(tools);
	init_validators(tools);
	init_shapes_name(tools);
}
