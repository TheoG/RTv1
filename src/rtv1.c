/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 16:23:29 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 16:38:43 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static t_color		get_pixel_color(t_object *obj, t_hit *hit, t_scene *scene,
						double *nearest)
{
	t_color		color;
	t_object	*light;

	color = int_to_rgb(scene->bg);
	light = scene->list_light;
	if (*nearest != INFINITY)
	{
		get_ambient(&color, obj, scene);
		while (light)
		{
			if (!get_shadow(scene, obj, hit, light))
			{
				get_diffuse(&color, obj, hit, light);
				get_specular(&color, obj, hit, light);
			}
			light = light->next;
		}
		c_clamp(&color, 0, 255);
	}
	return (color);
}

static int			cast_ray(t_vec3 orig, t_vec3 dir, t_scene *scene)
{
	t_object	*obj;
	t_object	*o_nearest;
	t_hit		inter;
	double		nearest;
	t_color		color;

	obj = scene->list_obj;
	nearest = INFINITY;
	o_nearest = NULL;
	inter.ray.orig = orig;
	inter.ray.dir = dir;
	while (obj)
	{
		inter.ray.t = INFINITY;
		if (obj->inter(obj, orig, dir, &inter) && inter.ray.t < nearest)
		{
			nearest = inter.ray.t;
			inter.hit = v3_add(orig, v3_prod(dir, inter.ray.t));
			inter.normal = v3_nrm(obj->get_normal(obj, &inter));
			o_nearest = obj;
		}
		obj = obj->next;
	}
	color = get_pixel_color(o_nearest, &inter, scene, &nearest);
	return (rgb_to_int(color));
}

static void			ray_trace(t_scene *scene)
{
	int		**pixelmap;
	double	ratio;
	t_pt2	i;
	t_vec3	dir;
	double	alpha;

	pixelmap = init_pixelmap(scene);
	ratio = scene->res.x / (double)scene->res.y;
	alpha = tan(to_radian(scene->list_camera->fov) / 2.0);
	i.y = -1;
	while (++i.y < scene->res.y)
	{
		i.x = -1;
		while (++i.x < scene->res.x)
		{
			dir.x = (2 * (i.x + 0.5) / scene->res.x - 1) * ratio * alpha;
			dir.y = (1 - 2 * (i.y + 0.5) / scene->res.y) * alpha;
			dir.z = DIST;
			dir = v3_m4_prod(v3_nrm(dir), scene->list_camera->m_ori);
			pixelmap[i.y][i.x] = cast_ray(scene->list_camera->pos, dir, scene);
		}
		display_percentage((int)(((double)i.y / scene->res.y) * 100) + 1);
	}
	sdl_render(pixelmap, scene->name, scene->res.x, scene->res.y);
	free_everything(scene, pixelmap);
}

void				rtv1(char *file)
{
	int			fd;
	t_list		*file_content;
	t_scene		*scene;
	t_rt		*tools;

	fd = open_file(file);
	if (!(file_content = read_file(fd)))
		ft_error_exit("Empty file.");
	close(fd);
	init_tools(&tools);
	if (!(scene = parse_file(file, file_content, tools)))
		ft_error_exit("Parsing error");
	ft_strcpy(scene->name, "RTv1: ");
	ft_strcat(scene->name, file);
	ray_trace(scene);
	free(tools);
}
