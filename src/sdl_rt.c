/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_rt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 16:35:38 by tgros             #+#    #+#             */
/*   Updated: 2017/03/12 17:00:50 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

SDL_Window	*init_sdl(SDL_Renderer **renderer, char *name, int x, int y)
{
	SDL_Window	*window;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		SDL_Quit();
		ft_error_exit("SDL error");
	}
	window = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED,
				SDL_WINDOWPOS_CENTERED, x, y, SDL_WINDOW_SHOWN);
	*renderer = SDL_CreateRenderer(window, -1, 0);
	if (!window)
	{
		SDL_Quit();
		ft_error_exit("SDL error");
	}
	return (window);
}

void		fill_renderer(int **pixelmap, SDL_Renderer *renderer, int x, int y)
{
	t_pt2	tab;
	t_color	col;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	tab.y = -1;
	while (++tab.y < y)
	{
		tab.x = -1;
		while (++tab.x < x)
		{
			col = int_to_rgb(pixelmap[tab.y][tab.x]);
			SDL_SetRenderDrawColor(renderer, col.r, col.g, col.b,
												SDL_ALPHA_OPAQUE);
			SDL_RenderDrawPoint(renderer, tab.x, tab.y);
		}
	}
}

int			check_event(SDL_Event *event)
{
	if (event->window.event == SDL_WINDOWEVENT_CLOSE)
		return (1);
	if (event->key.type == SDL_KEYDOWN)
	{
		if (event->key.keysym.sym == SDLK_ESCAPE)
			return (1);
	}
	return (0);
}

int			sdl_render(int **pixelmap, char *name, int x, int y)
{
	SDL_Event		event;
	SDL_Renderer	*renderer;
	SDL_Window		*window;
	int				done;

	done = 0;
	window = init_sdl(&renderer, name, x, y);
	fill_renderer(pixelmap, renderer, x, y);
	SDL_RenderPresent(renderer);
	while (!done)
	{
		SDL_WaitEvent(&event);
		done = check_event(&event);
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return (0);
}
