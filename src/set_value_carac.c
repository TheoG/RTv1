/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_value_carac.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 09:50:16 by tgros             #+#    #+#             */
/*   Updated: 2017/03/16 12:43:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	set_color(t_object *object, char *data, int line)
{
	int	color;

	color = hex_to_int(data);
	object->color = int_to_rgb(color);
	if (color > 16777215 || color < 0)
		ft_error_exit_line("Bad color value", line);
}

void	set_radius(t_object *object, char *data, int line)
{
	object->radius = ft_atof(data);
	if (object->radius < 0.0)
		ft_error_exit_line("Negative radius", line);
	if (object->radius == 0.0)
		ft_error_exit_line("Radius equals 0", line);
}

void	set_height(t_object *object, char *data, int line)
{
	object->height = ft_atof(data);
	if (object->height < 0.0)
	{
		object->height = 1.0f;
		ft_putstr("Negative height clamped to 1.0 at line ");
		ft_putnbr(line);
		ft_putendl("");
	}
}

void	set_intensity(t_object *object, char *data, int line)
{
	object->intensity = ft_atof(data);
	if (object->intensity < 0)
		ft_error_exit_line("Negative intensity", line);
	if (object->intensity == 0.0)
		ft_error_exit_line("Null intensity", line);
}

void	set_fov(t_object *object, char *data, int line)
{
	object->fov = ft_atof(data);
	if (object->fov < 0 || object->fov >= 180)
		ft_error_exit_line("Bad F.O.V value", line);
}
