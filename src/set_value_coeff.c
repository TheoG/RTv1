/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_value_coeff.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 09:51:20 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:55:54 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	set_kd(t_object *object, char *data, int line)
{
	object->kd = ft_atof(data);
	if (object->kd > 1.0 || object->kd < 0.0)
	{
		object->kd = f_clamp(object->kd, 0.0, 1.0f);
		ft_putstr("Bad diffuse value lamped between 0.0 and 1.0 at line ");
		ft_putnbr(line);
		ft_putendl("");
	}
}

void	set_ks(t_object *object, char *data, int line)
{
	object->ks = ft_atof(data);
	if (object->ks > 1.0 || object->ks < 0.0)
	{
		object->ks = f_clamp(object->ks, 0.0, 1.0f);
		ft_putstr("Bad specular value clamped between 0.0 and 1.0 at line ");
		ft_putnbr(line);
		ft_putendl("");
	}
}

void	set_spec_exp(t_object *object, char *data, int line)
{
	object->spec_exp = ft_atof(data);
	if (object->spec_exp < 2 || object->spec_exp > 1250)
	{
		object->spec_exp = f_clamp(object->spec_exp, 2, 1250);
		ft_putstr("Bad specular exponent clamped between 2 and 1250 at line ");
		ft_putnbr(line);
		ft_putendl("");
	}
}
