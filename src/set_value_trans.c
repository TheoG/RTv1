/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_value_trans.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 16:37:50 by tgros             #+#    #+#             */
/*   Updated: 2017/03/12 17:04:59 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	set_position(t_object *object, char *data, int line)
{
	t_vec3	pos;

	(void)line;
	pos = read_vec3(data);
	object->pos = pos;
}

void	set_direction(t_object *object, char *data, int line)
{
	t_vec3	dir;

	(void)line;
	dir = read_vec3(data);
	object->dir = dir;
}

void	set_rotation(t_object *object, char *data, int line)
{
	t_vec3	rot;

	(void)line;
	rot = read_vec3(data);
	rot.x > 360 ? rot.x = (int)rot.x % 360 : 0;
	rot.y > 360 ? rot.y = (int)rot.y % 360 : 0;
	rot.z > 360 ? rot.z = (int)rot.z % 360 : 0;
	rot.x < -360 ? rot.x = (int)rot.x % -360 : 0;
	rot.y < -360 ? rot.y = (int)rot.y % -360 : 0;
	rot.z < -360 ? rot.z = (int)rot.z % -360 : 0;
	object->rot = rot;
}

void	check_values(t_scene *scene)
{
	if (scene->res.x < 10 || scene->res.y < 10)
		ft_error_exit("Resolution too low.");
	if (scene->res.x > MAX_X || scene->res.y > MAX_Y)
		ft_error_exit("Resolution too high");
	if (!(scene->list_camera))
		ft_error_exit("At least one camera is required");
	if (!(scene->list_light))
		ft_error_exit("At least one light is required");
	if (!(scene->list_obj))
		ft_error_exit("At least one object is required");
	scene->ka = f_clamp(scene->ka, 0, 1);
	add_caps(scene);
}
