/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shading.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 12:59:32 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:55:38 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		get_ambient(t_color *c, t_object *obj, t_scene *scene)
{
	c->r = obj->color.r * scene->ka;
	c->g = obj->color.g * scene->ka;
	c->b = obj->color.b * scene->ka;
	return (1);
}

/*
**	Modifies the current pixel color to add the diffuse at the given hit point.
**	@param c: color
**	@param obj: the object hitten
**	@param hit: the struct t_hit that contains all the inter infos required
**	@param l: the light on the scene
**	@return The new color
*/

int		get_diffuse(t_color *color, t_object *obj, t_hit *hit, t_object *light)
{
	t_vec3	light_dir;
	double	diffuse;
	double	r2;

	r2 = v3_dist(hit->hit, light->pos);
	light_dir = v3_nrm(v3_sub(light->pos, hit->hit));
	diffuse = f_clamp(obj->kd * double_max(0.0, v3_dot(light_dir, hit->normal))
				* (light->intensity / r2), 0, 1);
	color->r += (diffuse * (light->color.r / 255.0)) * obj->color.r;
	color->g += (diffuse * (light->color.g / 255.0)) * obj->color.g;
	color->b += (diffuse * (light->color.b / 255.0)) * obj->color.b;
	return (1);
}

/*
**	Computes if the given object is in shadow
**	@param scene: The scene
**	@param obj: the hitten object
**	@param hit: The hit point
**	@param l: The light tested
**	@return true if in shadow, false if not
*/

int		get_shadow(t_scene *scene, t_object *obj, t_hit *hit, t_object *light)
{
	t_vec3		light_dir;
	t_object	*obj_t;
	double		dist;
	t_hit		i;

	light_dir = v3_sub(light->pos, v3_add(hit->hit, v3_prod(hit->hit, BIAIS)));
	dist = v3_len(light_dir);
	light_dir = v3_nrm(light_dir);
	obj_t = scene->list_obj;
	while (obj_t)
	{
		if (obj != obj_t && obj_t->inter(obj_t, hit->hit, light_dir, &i) &&
				i.ray.t > 0.0 && i.ray.t < dist)
			return (1);
		obj_t = obj_t->next;
	}
	return (0);
}

/*
**	Modifies the current pixel color to add the specular aspect at the
**	given hit point.
**	@param c: color
**	@param obj: the object hitten
**	@param hit: the struct t_hit that contains all the inter infos required
**	@param scene: the complete scene
**	@return The new color
*/

int		get_specular(t_color *c, t_object *obj, t_hit *hit, t_object *light)
{
	t_vec3	h;
	t_vec3	v;
	t_vec3	l;
	double	spec;
	double	r2;

	r2 = v3_dist(hit->hit, light->pos);
	l = v3_nrm(v3_sub(light->pos, hit->hit));
	v = v3_nrm(v3_sub(hit->ray.orig, hit->hit));
	h = v3_prod(v3_add(v, l), (1.0 / v3_len(v3_add(v, l))));
	spec = f_clamp(obj->ks * pow(double_max(0.0, v3_dot(hit->normal, h)),
				obj->spec_exp) * (light->intensity / r2), 0, 1);
	c->r += spec * light->color.r;
	c->g += spec * light->color.g;
	c->b += spec * light->color.b;
	return (1);
}
