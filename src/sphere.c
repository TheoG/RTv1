/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 11:07:08 by tgros             #+#    #+#             */
/*   Updated: 2017/03/15 15:55:24 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		inter_sphere(t_object *obj, t_vec3 orig, t_vec3 dir, t_hit *t)
{
	t_vec3	u;
	t_vec2	res;
	t_vec3	ori_cen;

	ori_cen = v3_sub(orig, obj->pos);
	u.x = v3_dot(dir, dir);
	u.y = 2.0 * v3_dot(dir, ori_cen);
	u.z = v3_dot(ori_cen, ori_cen) - (obj->radius * obj->radius);
	if (!quadratic(u.x, u.y, u.z, &res))
		return (0);
	if (res.x > res.y)
		double_swap(&(res.x), &(res.y));
	if (res.x < 0.0)
	{
		res.x = res.y;
		if (res.y < 0.0)
			return (0);
	}
	t->ray.t = res.x;
	return (1);
}

t_vec3	norme_sphere(t_object *obj, t_hit *hit)
{
	t_vec3	n;
	int		n_dir;

	n = v3_prod(v3_sub(hit->hit, obj->pos), obj->n_dir);
	n_dir = v3_dot(n, hit->ray.dir) > 0 ? -1 : 1;
	return (v3_prod(n, n_dir));
}
