/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unit_parser.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 13:49:30 by tgros             #+#    #+#             */
/*   Updated: 2017/03/02 15:00:01 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vec3				read_vec3(char *str)
{
	char	**split;
	t_vec3	vec;

	if (!str)
	{
		vec.x = 0;
		vec.y = 0;
		vec.z = 0;
		return (vec);
	}
	split = ft_strsplit(str, ',');
	if (ft_strarraylength(split) != 3)
		ft_error_exit("Bad vector format");
	vec.x = ft_atof(split[0]);
	vec.y = ft_atof(split[1]);
	vec.z = ft_atof(split[2]);
	free_split(split);
	return (vec);
}

t_pt2				read_pt2(char *str)
{
	char	**split;
	t_pt2	vec;

	if (!str)
	{
		vec.x = 0;
		vec.y = 0;
		return (vec);
	}
	split = ft_strsplit(str, ',');
	if (ft_strarraylength(split) != 2)
		ft_error_exit("Bad vector format");
	vec.x = ft_atoi(split[0]);
	vec.y = ft_atoi(split[1]);
	free_split(split);
	return (vec);
}
