/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <tgros@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 10:26:15 by tgros             #+#    #+#             */
/*   Updated: 2017/03/12 17:05:20 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		ft_strarraylength(char *tab[])
{
	int res;

	res = 0;
	while (*tab++)
		res++;
	return (res);
}

void	display_percentage(int percent)
{
	ft_putstr(" Please wait...");
	ft_putnbr(percent);
	ft_putstr("%\r");
}
